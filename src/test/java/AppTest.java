import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class AppTest {
	public static final Logger logger = LogManager.getLogger(AppTest.class);

	@Test(priority = 1)
	public void getRequest() {
		String url = "https://reqres.in/api/users?page=2";
		RestAssured.baseURI = url;
		RequestSpecification requestSpecification = RestAssured.given();
		Response response = requestSpecification.accept(ContentType.JSON).get();
		System.out.println(response.asPrettyString());
		Assert.assertEquals(response.getStatusCode(), 200);
		logger.debug("This is a get debug level log !");
	}

	@Test(priority = 2)
	public void postRequest() {
		String api = "https://reqres.in/api/users";
		String payload = "{\n" + " \"name\": \"Ojasvii\",\n" + " \"job\": \"Automation Engineer\"\n" + "}";
		RestAssured.baseURI = api;
		RequestSpecification requestSpecification = RestAssured.given();
		requestSpecification.body(payload).post().then().log().body().assertThat().statusCode(201);
		logger.debug("This is a post debug level log !");
	}
}